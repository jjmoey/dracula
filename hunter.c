////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// hunter.c: your "Fury of Dracula" hunter AI.
//
// 2014-07-01   v1.0    Team Dracula <cs2521@cse.unsw.edu.au>
// 2017-12-01   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>

#include "Game.h"
#include "HunterView.h"
#include "hunter.h"
#include "Graph.h"

#define PLAYER1_START_POS MADRID
#define PLAYER2_START_POS LONDON
#define PLAYER3_START_POS AMSTERDAM
#define PLAYER4_START_POS BUCHAREST

#define PRIOR_NORMAL 0
#define PRIOR_LEADER 1
#define TURNS_UNTIL_RESEARCH 6

static LocationID makeRandomMove(HunterView hv);
static LocationID makePlayerMove(HunterView hv, char *out);

static int currPlayer;
static int turnsSinceTrailLost;
static LocationID lastKnownLoc;


/*
    General strategy:
        if Dracula's trail not found, pick random location
        [TBA] if players are sufficiently close together, explode
        [TBA] Do not move into the past 3 locations in player's trail
        Rest every 6 turns to reveal Dracula's 6th position
        [TBA] Only update the counter if Dracula's trail is lost

        if Dracula's trail found, converge on said location
        [TBA] also pick adjacent location to lock to in case central location is lost
        [TBA] communicate to other players where target is

*/

void
decideHunterMove (HunterView hv)
{
    srand(time(NULL));

    currPlayer = whoAmI(hv);
    int currRound = giveMeTheRound(hv);
    // priority system
    PlayerMessage message = "";

    // implement some priority system
    // to track trail of player closest to dracula
    int playerTrail[TRAIL_SIZE];
    giveMeTheTrail(hv, currPlayer, playerTrail);
    // print trail;
    printf("LT->");
	int j;
	for (j=0;j<TRAIL_SIZE;j++){
		printf("[%s]->",idToName(playerTrail[j]));
	}
	printf("[x]\n");

    // get dracula's trail;
    int dracTrail[TRAIL_SIZE];
    giveMeTheTrail(hv, PLAYER_DRACULA, dracTrail);
    // print
    printf("DT->");
	int m;
	for (m=0;m<TRAIL_SIZE;m++){
		printf("[%s]->",idToName(dracTrail[m]));
	}
	printf("[x]\n");

	printf("\n");

    int move = UNKNOWN_LOCATION;

    if (currRound == 0) {
		if(currPlayer == PLAYER_LORD_GODALMING) {move = PLAYER1_START_POS;}
		else if(currPlayer == PLAYER_DR_SEWARD) {move = PLAYER2_START_POS;}
		else if(currPlayer == PLAYER_VAN_HELSING) {move = PLAYER3_START_POS;}
		else if(currPlayer == PLAYER_MINA_HARKER) {move = PLAYER4_START_POS;}
		else printf("unknown player\n");
		lastKnownLoc = UNKNOWN_LOCATION;
	} else {
        move = makePlayerMove(hv, message);
    }

    printf("last known location: %s (%d))\n", idToName(lastKnownLoc), lastKnownLoc);
    char * moveTo = idToAbbrev(move);


    registerBestPlay (moveTo, "I'm on holiday in Geneva");
}

LocationID makeRandomMove(HunterView hv) {
    int numLocations;
	int *locs = whereCanIgo(hv,&numLocations,true,true,true);

    printf("locs (#locs = %d) ", numLocations);
    for (int i = 0; i < numLocations; i++) {
        printf("[%s] - ", idToName(locs[i]));
    }
    printf("\n");
	int r = (rand()%(numLocations-1))+1; //so we don't select current loc (element 0)
    if (locs != NULL && locs[r] != UNKNOWN_LOCATION) {
        // printf("eer %s\n", idToName(locs[r]));
	    return locs[r];
    } else {
        return whereIs(hv, whoAmI(hv));
    }
}

LocationID makePlayerMove(HunterView hv, char *out) {
    LocationID move;
    out = "Message";
    int dracTrail[TRAIL_SIZE];

    int ploc = whereIs(hv, currPlayer);

    LocationID dracLoc = whereIs (hv, PLAYER_DRACULA);
    // find the most recent location in Dracula's trail
    giveMeTheTrail(hv, PLAYER_DRACULA, dracTrail);

    printf("trail detect: \n");
    int latest = -1;
    for (int i = 5; i >= 0; i--) {
        if (dracTrail[i] >= MIN_MAP_LOCATION && dracTrail[i] <= MAX_MAP_LOCATION) {
            latest = i;
            lastKnownLoc = dracTrail[i];
        } else if (dracTrail[i] == TELEPORT) {
            latest = i;
            lastKnownLoc = CASTLE_DRACULA;
        } else { 
            lastKnownLoc = UNKNOWN_LOCATION;
        }
        printf("dracTrail[%d]: %s (%d); lastknown: %d\n", i, idToName(dracTrail[i]), dracTrail[i], lastKnownLoc);
    }
    printf("\n");


    // decide move here
    /*
        if location exists, find path to location and start moving
        if not, default to random movement
    */

    if (dracLoc >= MIN_MAP_LOCATION && dracLoc <= MAX_MAP_LOCATION) {
        int path[NUM_MAP_LOCATIONS];
        int plength = findHunterPath(hv, ploc, dracLoc, path, true, true, true);

        printf("path: ");
        for (int i = 0; i < plength; i++) {
            printf("[%s]->", idToName(path[i]) );
        }
        printf("x\n\n") ;

        move = path[1];
        lastKnownLoc = dracLoc;
        turnsSinceTrailLost = 0;
    }
    else if (latest > -1) {

        printf("latest trail: %d\n", latest);
        int path[NUM_MAP_LOCATIONS];
        int plength = findHunterPath(hv, ploc, dracTrail[latest], path, true, true, true);

        printf("path: ");
        for (int i = 0; i < plength; i++) {
            printf("[%s]->", idToName(path[i]) );
        }
        printf("x\n\n") ;

        move = path[1];
        // move = makeRandomMove(hv);

        printf("[found trail] ");
        turnsSinceTrailLost = 0;
    } else if (latest == -1 && turnsSinceTrailLost < TURNS_UNTIL_RESEARCH &&
        (lastKnownLoc >= MIN_MAP_LOCATION && lastKnownLoc <= MAX_MAP_LOCATION) && ploc != lastKnownLoc
    ) {
        int path[NUM_MAP_LOCATIONS];
        int plength = findHunterPath(hv, ploc, lastKnownLoc, path, true, true, true);

        printf("path: ");
        for (int i = 0; i < plength; i++) {
            printf("[%s]->", idToName(path[i]) );
        }
        printf("x\n\n") ;

        move = path[1];
        printf("[lost trail] ");
        turnsSinceTrailLost++;

    } else if (turnsSinceTrailLost == TURNS_UNTIL_RESEARCH) {
        move = whereIs(hv, currPlayer);
        printf("[resting - lost trail] ");
        lastKnownLoc = UNKNOWN_LOCATION;
        turnsSinceTrailLost = 0;
    } else if (howHealthyIs(hv, currPlayer) < 5) {
        move = whereIs(hv, currPlayer);

        printf("[resting - low health] ");
    } else {
        move = makeRandomMove(hv);
        printf("[random] ");
        turnsSinceTrailLost++;
    }
    printf("moving to: %s\n", idToName(move));

    return move;
}
