#include <stdbool.h>
#include "Places.h"
// Queue.h
typedef LocationID Item;

typedef struct queueImp *Queue;
//Function Prototypes
Queue createQueue( void );
void destroyQueue( Queue queue );
Item deQueue( Queue queue);
Item getHead( Queue queue);
Item getTail( Queue queue);
void enQueue( Queue queue, Item data);
void printQueueItem(Item item);
void traverseQueue(Queue queue);
Queue copyQueue(Queue queue);
int queueSize( Queue queue);
bool queueFind(Queue queue, LocationID item);
bool queueHasItems(Queue queue);
