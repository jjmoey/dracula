// An interface for a weighted graph!

#ifndef GRAPH_H
#define GRAPH_H

// vertices denoted by integers 0..N-1
typedef int Vertex;

// edges are pairs of vertices (end-points)
typedef struct {
    Vertex v;
    Vertex w;
    int weight;
} GEdge;

GEdge mkEdge(Vertex, Vertex, int);

// graph representation is hidden
typedef struct graphRep *Graph;

// operations on graphs
Graph newGraph(int nV); // #vertices
void insertEdge(Graph g, GEdge e);
void removeEdge(Graph g, GEdge e);

//returns 1 if there is ad adge from v to w
int isAdjacent(Graph g,Vertex v, Vertex w);
int adjacentVertices(Graph g, Vertex v, Vertex adj[]);
int adjacentEdges(Graph g, Vertex v, GEdge adj[]);


int numVertices(Graph g);
int numEdges(Graph g);

Graph copy(Graph g);
void destroyGraph(Graph g);
void showGraph(Graph g);

int findPath(Graph g, Vertex src, Vertex dest, int max, int *path);
int findDist(Graph g, Vertex src, Vertex dest);


void addConnections(Graph g);
void addRoadConnections(Graph g);
void addRoadSeaConnections(Graph g); 
#endif
