//An adjacency matrix representation of a directed, weighted graph

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <stdbool.h>
#include "Graph.h"
// #include "Item.h"
#include "Queue.h"
#include "Places.h"
#include "Map.h"

#define NO_EDGE FLT_MAX

typedef struct graphRep GraphRep;

struct graphRep {
    int nV; // #vertices
    int nE; // #adj
    float **adj; //matrix of weights
};

static int validV(Graph g, Vertex v) ;
static void addTransportLink(Graph g, LocationID v, LocationID w, int wt);

static void addTransportLink(Graph g, LocationID v, LocationID w, int wt) {
    GEdge edge = mkEdge(v, w, wt);

    insertEdge(g, edge);
}



// local checking function
static int validV(Graph g, Vertex v) {
    return (v >= 0 && v < g->nV);
}

//returns 1 if there is an edge from v to w
//O(1)
int isAdjacent(Graph g, Vertex v, Vertex w){
    assert(g != NULL && validV(g,v) && validV(g,w));
    if(g->adj[v][w] == NO_EDGE) return 0;
    return 1;
}

//returns the number of adjacent vertices
//O(V)
int adjacentVertices(Graph g, Vertex v, Vertex adj[]){
    int counter=0;
    int i=0;
    for(i=0;i<g->nV;i++){
        if(g->adj[v][i] != NO_EDGE){
            adj[counter]=i;
            counter++;
        }
    }
    return counter;
}

int adjacentEdges(Graph g, Vertex v, GEdge adj[]){
    int counter=0;
    int i=0;
    for(i=0;i<g->nV;i++){
        if(g->adj[v][i] != NO_EDGE){
            adj[counter]=mkEdge(v,i,g->adj[v][i]);
            counter++;
        }
    }
    return counter;

}

// Create an edge from v to w
// Allows self loops
GEdge mkEdge(Vertex v, Vertex w, int weight) {
    assert(v >= 0 && w >= 0);
    GEdge e = {v,w,weight};
    return e;
}

//Initialise a new graph
Graph newGraph(int nV) {
    assert(nV >= 0);
    int i, j;
    Graph g = malloc(sizeof(GraphRep));
    assert(g != NULL);
    g->adj = malloc(nV * sizeof(int *));
    assert(g->adj != NULL);
    for (i = 0; i < nV; i++) {
        g->adj[i] = malloc(nV * sizeof(int));
        assert(g->adj[i] != NULL);
        for (j = 0; j < nV; j++) g->adj[i][j] = NO_EDGE;
    }
    g->nV = nV;
    g->nE = 0;
    return g;
}

//Insert an edge into a graph
void insertEdge(Graph g, GEdge e) {
    assert(g != NULL && validV(g,e.v) && validV(g,e.w));
    if ( !(g->adj[e.v][e.w]== NO_EDGE) )
        return;
    g->adj[e.v][e.w] = e.weight;
    g->adj[e.w][e.v] = e.weight;
    g->nE++;
}

// remove an edge from a graph
void removeEdge(Graph g, GEdge e) {
   assert(g != NULL && validV(g,e.v)&& validV(g,e.w));
   if (g->adj[e.v][e.w] == NO_EDGE)
       return;
   g->adj[e.v][e.w] = NO_EDGE;
   g->adj[e.w][e.v] = NO_EDGE;
   g->nE--;
}

//Display the graph
void show(Graph g) {
    assert(g != NULL);
    printf("V=%d, E=%d\n", g->nV, g->nE);
    int i, j;
    for (i = 0; i < g->nV; i++) {
        int nshown = 0;
        for (j = 0; j < g->nV; j++) {
            if (g->adj[i][j] != NO_EDGE) {
                printf("%d-%d : %lf ",i,j,g->adj[i][j]);
                nshown++;
            }
        }
        if (nshown > 0) printf("\n");
    }
}


int numVertices(Graph g){
    assert(g != NULL);
    return g->nV;
}

int numEdges(Graph g){
    assert(g != NULL);
    return g->nE;
}


void printMST(int numV,int st[],float dist[]){
  int i;
  double total = 0;
   for(i=1;i<numV;i++){
     printf("%d %d %f\n",st[i],i,dist[i]);
     total+=dist[i];
   }
   printf("Total %f\n",total);
}

//Display the graph
void showGraph(Graph g) {
    assert(g != NULL);
    printf("V=%d, E=%d\n", g->nV, g->nE);
    int i, j;
    for (i = 0; i < g->nV; i++) {
        int nshown = 0;
        for (j = 0; j < g->nV; j++) {
            if (g->adj[i][j] != NO_EDGE) {
                printf("%d-%d %.2f ",i,j,g->adj[i][j]);
                nshown++;
            }
        }
        if (nshown > 0) {
           printf("\n");
        }
    }
}

void destroyGraph(Graph g){
    assert(g != NULL);

	int i;
	for (i = 0; i < g->nV; i++) {
		free(g->adj[i]);
	}

	free(g);
}

int findPath(Graph g, Vertex src, Vertex dest, int max, int *path) {
    assert(g != NULL && validV(g,src) && validV(g,dest));
	int temp_city = src;
	int index = 0;
	// Temporary store of path_distance for calculations
	int temp_distance = 0;

	int path_distance = 0;

	// Array of visited cities, if not visited 0, else 1
	int visited[NUM_MAP_LOCATIONS] = {0};

	// Stores index of the previous city, default value -1
	int prev[NUM_MAP_LOCATIONS] = {[0 ... (NUM_MAP_LOCATIONS-1)] = -1};

	Queue cityQ = createQueue();
	enQueue(cityQ, src);

	// While Queue is not empty and the temp_city is not the destination city (e.g. when path to destination city from src is found)
	while (queueHasItems(cityQ) == true && temp_city != dest) {
		temp_city = deQueue(cityQ);

		// Checks through all potential connected cities
		while (index < g->nV) {
			// Checking if the distance between the two cities is less than the specified maximum and the city being checked is not the destination
			if (g->adj[temp_city][index] > 0 && g->adj[temp_city][index] < max && visited[index] != 1 && temp_city != dest) {
				enQueue(cityQ, index);
				prev[index] = temp_city;
				visited[index] = 1;

			// If city is found
			} else if (temp_city == dest) {
				prev[index] = temp_city;

				// Calculating size of path
				while (index != src) {
					index = prev[index];
					path_distance++;
				}

				// Building path array, storing destination first
				temp_distance = path_distance-1;
				path[temp_distance] = dest;
				temp_distance--;

				// Storing rest of array
				index = prev[dest];
				while (temp_distance >= 0) {
					path[temp_distance] = index;
					index = prev[index];
					temp_distance--;
				}

				break;
			}

			index++;
		}

		index = 0;
	}

	return path_distance;
}

int findDist(Graph g, Vertex src, Vertex dest) {
    // printf("graph %p\n", g); 
    assert(g != NULL && validV(g,src) && validV(g,dest));
	Item tmp_city = src;
	int index = 0;

	int distance = 0;
	int visited[NUM_MAP_LOCATIONS] = {0};
	int prev[NUM_MAP_LOCATIONS] = {[0 ... (NUM_MAP_LOCATIONS-1)] = -1};

	Queue cityQ = createQueue();
	enQueue(cityQ, src);

	// While Queue is not empty and the tmp_city is not the destination city (e.g. when path to destination city from src is found)
	while (queueHasItems(cityQ) == true && tmp_city != dest) {
		tmp_city = deQueue(cityQ);

		// Checks through all potential connected cities
		while (index < g->nV) {
			// Checking if the distance between the two cities is less than the specified maximum and the city being checked is not the destination
			if (g->adj[tmp_city][index] > 1 && g->adj[tmp_city][index] > 4 && g->adj[tmp_city][index] >6
						&& g->adj[tmp_city][index] >7 && visited[index] != 1 && tmp_city != dest) {
				enQueue(cityQ, index);
				prev[index] = tmp_city;
				visited[index] = 1;

			// If city is found
			} else if (tmp_city == dest) {
				prev[index] = tmp_city;

				// Calculating size of path
				while (index != src) {
					index = prev[index];
					distance++;
				}

				break;
			}

			index++;
		}

		index = 0;
	}

	return distance;
}

// builds graph with all connections
void addConnections(Graph g)
{
    //### ROAD Connections ###

    addTransportLink(g, ALICANTE, GRANADA, ROAD);
    addTransportLink(g, ALICANTE, MADRID, ROAD);
    addTransportLink(g, ALICANTE, SARAGOSSA, ROAD);
    addTransportLink(g, AMSTERDAM, BRUSSELS, ROAD);
    addTransportLink(g, AMSTERDAM, COLOGNE, ROAD);
    addTransportLink(g, ATHENS, VALONA, ROAD);
    addTransportLink(g, BARCELONA, SARAGOSSA, ROAD);
    addTransportLink(g, BARCELONA, TOULOUSE, ROAD);
    addTransportLink(g, BARI, NAPLES, ROAD);
    addTransportLink(g, BARI, ROME, ROAD);
    addTransportLink(g, BELGRADE, BUCHAREST, ROAD);
    addTransportLink(g, BELGRADE, KLAUSENBURG, ROAD);
    addTransportLink(g, BELGRADE, SARAJEVO, ROAD);
    addTransportLink(g, BELGRADE, SOFIA, ROAD);
    addTransportLink(g, BELGRADE, ST_JOSEPH_AND_ST_MARYS, ROAD);
    addTransportLink(g, BELGRADE, SZEGED, ROAD);
    addTransportLink(g, BERLIN, HAMBURG, ROAD);
    addTransportLink(g, BERLIN, LEIPZIG, ROAD);
    addTransportLink(g, BERLIN, PRAGUE, ROAD);
    addTransportLink(g, BORDEAUX, CLERMONT_FERRAND, ROAD);
    addTransportLink(g, BORDEAUX, NANTES, ROAD);
    addTransportLink(g, BORDEAUX, SARAGOSSA, ROAD);
    addTransportLink(g, BORDEAUX, TOULOUSE, ROAD);
    addTransportLink(g, BRUSSELS, COLOGNE, ROAD);
    addTransportLink(g, BRUSSELS, LE_HAVRE, ROAD);
    addTransportLink(g, BRUSSELS, PARIS, ROAD);
    addTransportLink(g, BRUSSELS, STRASBOURG, ROAD);
    addTransportLink(g, BUCHAREST, CONSTANTA, ROAD);
    addTransportLink(g, BUCHAREST, GALATZ, ROAD);
    addTransportLink(g, BUCHAREST, KLAUSENBURG, ROAD);
    addTransportLink(g, BUCHAREST, SOFIA, ROAD);
    addTransportLink(g, BUDAPEST, KLAUSENBURG, ROAD);
    addTransportLink(g, BUDAPEST, SZEGED, ROAD);
    addTransportLink(g, BUDAPEST, VIENNA, ROAD);
    addTransportLink(g, BUDAPEST, ZAGREB, ROAD);
    addTransportLink(g, CADIZ, GRANADA, ROAD);
    addTransportLink(g, CADIZ, LISBON, ROAD);
    addTransportLink(g, CADIZ, MADRID, ROAD);
    addTransportLink(g, CASTLE_DRACULA, GALATZ, ROAD);
    addTransportLink(g, CASTLE_DRACULA, KLAUSENBURG, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, GENEVA, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, MARSEILLES, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, NANTES, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, PARIS, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, TOULOUSE, ROAD);
    addTransportLink(g, COLOGNE, FRANKFURT, ROAD);
    addTransportLink(g, COLOGNE, HAMBURG, ROAD);
    addTransportLink(g, COLOGNE, LEIPZIG, ROAD);
    addTransportLink(g, COLOGNE, STRASBOURG, ROAD);
    addTransportLink(g, CONSTANTA, GALATZ, ROAD);
    addTransportLink(g, CONSTANTA, VARNA, ROAD);
    addTransportLink(g, DUBLIN, GALWAY, ROAD);
    addTransportLink(g, EDINBURGH, MANCHESTER, ROAD);
    addTransportLink(g, FLORENCE, GENOA, ROAD);
    addTransportLink(g, FLORENCE, ROME, ROAD);
    addTransportLink(g, FLORENCE, VENICE, ROAD);
    addTransportLink(g, FRANKFURT, LEIPZIG, ROAD);
    addTransportLink(g, FRANKFURT, NUREMBURG, ROAD);
    addTransportLink(g, FRANKFURT, STRASBOURG, ROAD);
    addTransportLink(g, GALATZ, KLAUSENBURG, ROAD);
    addTransportLink(g, GENEVA, MARSEILLES, ROAD);
    addTransportLink(g, GENEVA, PARIS, ROAD);
    addTransportLink(g, GENEVA, STRASBOURG, ROAD);
    addTransportLink(g, GENEVA, ZURICH, ROAD);
    addTransportLink(g, GENOA, MARSEILLES, ROAD);
    addTransportLink(g, GENOA, MILAN, ROAD);
    addTransportLink(g, GENOA, VENICE, ROAD);
    addTransportLink(g, GRANADA, MADRID, ROAD);
    addTransportLink(g, HAMBURG, LEIPZIG, ROAD);
    addTransportLink(g, KLAUSENBURG, SZEGED, ROAD);
    addTransportLink(g, LEIPZIG, NUREMBURG, ROAD);
    addTransportLink(g, LE_HAVRE, NANTES, ROAD);
    addTransportLink(g, LE_HAVRE, PARIS, ROAD);
    addTransportLink(g, LISBON, MADRID, ROAD);
    addTransportLink(g, LISBON, SANTANDER, ROAD);
    addTransportLink(g, LIVERPOOL, MANCHESTER, ROAD);
    addTransportLink(g, LIVERPOOL, SWANSEA, ROAD);
    addTransportLink(g, LONDON, MANCHESTER, ROAD);
    addTransportLink(g, LONDON, PLYMOUTH, ROAD);
    addTransportLink(g, LONDON, SWANSEA, ROAD);
    addTransportLink(g, MADRID, SANTANDER, ROAD);
    addTransportLink(g, MADRID, SARAGOSSA, ROAD);
    addTransportLink(g, MARSEILLES, MILAN, ROAD);
    addTransportLink(g, MARSEILLES, TOULOUSE, ROAD);
    addTransportLink(g, MARSEILLES, ZURICH, ROAD);
    addTransportLink(g, MILAN, MUNICH, ROAD);
    addTransportLink(g, MILAN, VENICE, ROAD);
    addTransportLink(g, MILAN, ZURICH, ROAD);
    addTransportLink(g, MUNICH, NUREMBURG, ROAD);
    addTransportLink(g, MUNICH, STRASBOURG, ROAD);
    addTransportLink(g, MUNICH, VENICE, ROAD);
    addTransportLink(g, MUNICH, VIENNA, ROAD);
    addTransportLink(g, MUNICH, ZAGREB, ROAD);
    addTransportLink(g, MUNICH, ZURICH, ROAD);
    addTransportLink(g, NANTES, PARIS, ROAD);
    addTransportLink(g, NAPLES, ROME, ROAD);
    addTransportLink(g, NUREMBURG, PRAGUE, ROAD);
    addTransportLink(g, NUREMBURG, STRASBOURG, ROAD);
    addTransportLink(g, PARIS, STRASBOURG, ROAD);
    addTransportLink(g, PRAGUE, VIENNA, ROAD);
    addTransportLink(g, SALONICA, SOFIA, ROAD);
    addTransportLink(g, SALONICA, VALONA, ROAD);
    addTransportLink(g, SANTANDER, SARAGOSSA, ROAD);
    addTransportLink(g, SARAGOSSA, TOULOUSE, ROAD);
    addTransportLink(g, SARAJEVO, SOFIA, ROAD);
    addTransportLink(g, SARAJEVO, ST_JOSEPH_AND_ST_MARYS, ROAD);
    addTransportLink(g, SARAJEVO, VALONA, ROAD);
    addTransportLink(g, SARAJEVO, ZAGREB, ROAD);
    addTransportLink(g, SOFIA, VALONA, ROAD);
    addTransportLink(g, SOFIA, VARNA, ROAD);
    addTransportLink(g, STRASBOURG, ZURICH, ROAD);
    addTransportLink(g, ST_JOSEPH_AND_ST_MARYS, SZEGED, ROAD);
    addTransportLink(g, ST_JOSEPH_AND_ST_MARYS, ZAGREB, ROAD);
    addTransportLink(g, SZEGED, ZAGREB, ROAD);
    addTransportLink(g, VIENNA, ZAGREB, ROAD);

    //### RAIL Connections ###

    addTransportLink(g, ALICANTE, BARCELONA, RAIL);
    addTransportLink(g, ALICANTE, MADRID, RAIL);
    addTransportLink(g, BARCELONA, SARAGOSSA, RAIL);
    addTransportLink(g, BARI, NAPLES, RAIL);
    addTransportLink(g, BELGRADE, SOFIA, RAIL);
    addTransportLink(g, BELGRADE, SZEGED, RAIL);
    addTransportLink(g, BERLIN, HAMBURG, RAIL);
    addTransportLink(g, BERLIN, LEIPZIG, RAIL);
    addTransportLink(g, BERLIN, PRAGUE, RAIL);
    addTransportLink(g, BORDEAUX, PARIS, RAIL);
    addTransportLink(g, BORDEAUX, SARAGOSSA, RAIL);
    addTransportLink(g, BRUSSELS, COLOGNE, RAIL);
    addTransportLink(g, BRUSSELS, PARIS, RAIL);
    addTransportLink(g, BUCHAREST, CONSTANTA, RAIL);
    addTransportLink(g, BUCHAREST, GALATZ, RAIL);
    addTransportLink(g, BUCHAREST, SZEGED, RAIL);
    addTransportLink(g, BUDAPEST, SZEGED, RAIL);
    addTransportLink(g, BUDAPEST, VIENNA, RAIL);
    addTransportLink(g, COLOGNE, FRANKFURT, RAIL);
    addTransportLink(g, EDINBURGH, MANCHESTER, RAIL);
    addTransportLink(g, FLORENCE, MILAN, RAIL);
    addTransportLink(g, FLORENCE, ROME, RAIL);
    addTransportLink(g, FRANKFURT, LEIPZIG, RAIL);
    addTransportLink(g, FRANKFURT, STRASBOURG, RAIL);
    addTransportLink(g, GENEVA, MILAN, RAIL);
    addTransportLink(g, GENOA, MILAN, RAIL);
    addTransportLink(g, LEIPZIG, NUREMBURG, RAIL);
    addTransportLink(g, LE_HAVRE, PARIS, RAIL);
    addTransportLink(g, LISBON, MADRID, RAIL);
    addTransportLink(g, LIVERPOOL, MANCHESTER, RAIL);
    addTransportLink(g, LONDON, MANCHESTER, RAIL);
    addTransportLink(g, LONDON, SWANSEA, RAIL);
    addTransportLink(g, MADRID, SANTANDER, RAIL);
    addTransportLink(g, MADRID, SARAGOSSA, RAIL);
    addTransportLink(g, MARSEILLES, PARIS, RAIL);
    addTransportLink(g, MILAN, ZURICH, RAIL);
    addTransportLink(g, MUNICH, NUREMBURG, RAIL);
    addTransportLink(g, NAPLES, ROME, RAIL);
    addTransportLink(g, PRAGUE, VIENNA, RAIL);
    addTransportLink(g, SALONICA, SOFIA, RAIL);
    addTransportLink(g, SOFIA, VARNA, RAIL);
    addTransportLink(g, STRASBOURG, ZURICH, RAIL);
    addTransportLink(g, VENICE, VIENNA, RAIL);

    //### BOAT Connections ###

    addTransportLink(g, ADRIATIC_SEA, BARI, BOAT);
    addTransportLink(g, ADRIATIC_SEA, IONIAN_SEA, BOAT);
    addTransportLink(g, ADRIATIC_SEA, VENICE, BOAT);
    addTransportLink(g, ALICANTE, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, AMSTERDAM, NORTH_SEA, BOAT);
    addTransportLink(g, ATHENS, IONIAN_SEA, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, BAY_OF_BISCAY, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, CADIZ, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, ENGLISH_CHANNEL, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, GALWAY, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, IRISH_SEA, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, LISBON, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, NORTH_SEA, BOAT);
    addTransportLink(g, BARCELONA, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, BAY_OF_BISCAY, BORDEAUX, BOAT);
    addTransportLink(g, BAY_OF_BISCAY, NANTES, BOAT);
    addTransportLink(g, BAY_OF_BISCAY, SANTANDER, BOAT);
    addTransportLink(g, BLACK_SEA, CONSTANTA, BOAT);
    addTransportLink(g, BLACK_SEA, IONIAN_SEA, BOAT);
    addTransportLink(g, BLACK_SEA, VARNA, BOAT);
    addTransportLink(g, CAGLIARI, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, CAGLIARI, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, DUBLIN, IRISH_SEA, BOAT);
    addTransportLink(g, EDINBURGH, NORTH_SEA, BOAT);
    addTransportLink(g, ENGLISH_CHANNEL, LE_HAVRE, BOAT);
    addTransportLink(g, ENGLISH_CHANNEL, LONDON, BOAT);
    addTransportLink(g, ENGLISH_CHANNEL, NORTH_SEA, BOAT);
    addTransportLink(g, ENGLISH_CHANNEL, PLYMOUTH, BOAT);
    addTransportLink(g, GENOA, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, HAMBURG, NORTH_SEA, BOAT);
    addTransportLink(g, IONIAN_SEA, SALONICA, BOAT);
    addTransportLink(g, IONIAN_SEA, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, IONIAN_SEA, VALONA, BOAT);
    addTransportLink(g, IRISH_SEA, LIVERPOOL, BOAT);
    addTransportLink(g, IRISH_SEA, SWANSEA, BOAT);
    addTransportLink(g, MARSEILLES, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, MEDITERRANEAN_SEA, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, NAPLES, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, ROME, TYRRHENIAN_SEA, BOAT);
}


// builds graph without rail connections
void addRoadSeaConnections(Graph g)
{
    //### ROAD Connections ###

    addTransportLink(g, ALICANTE, GRANADA, ROAD);
    addTransportLink(g, ALICANTE, MADRID, ROAD);
    addTransportLink(g, ALICANTE, SARAGOSSA, ROAD);
    addTransportLink(g, AMSTERDAM, BRUSSELS, ROAD);
    addTransportLink(g, AMSTERDAM, COLOGNE, ROAD);
    addTransportLink(g, ATHENS, VALONA, ROAD);
    addTransportLink(g, BARCELONA, SARAGOSSA, ROAD);
    addTransportLink(g, BARCELONA, TOULOUSE, ROAD);
    addTransportLink(g, BARI, NAPLES, ROAD);
    addTransportLink(g, BARI, ROME, ROAD);
    addTransportLink(g, BELGRADE, BUCHAREST, ROAD);
    addTransportLink(g, BELGRADE, KLAUSENBURG, ROAD);
    addTransportLink(g, BELGRADE, SARAJEVO, ROAD);
    addTransportLink(g, BELGRADE, SOFIA, ROAD);
    addTransportLink(g, BELGRADE, ST_JOSEPH_AND_ST_MARYS, ROAD);
    addTransportLink(g, BELGRADE, SZEGED, ROAD);
    addTransportLink(g, BERLIN, HAMBURG, ROAD);
    addTransportLink(g, BERLIN, LEIPZIG, ROAD);
    addTransportLink(g, BERLIN, PRAGUE, ROAD);
    addTransportLink(g, BORDEAUX, CLERMONT_FERRAND, ROAD);
    addTransportLink(g, BORDEAUX, NANTES, ROAD);
    addTransportLink(g, BORDEAUX, SARAGOSSA, ROAD);
    addTransportLink(g, BORDEAUX, TOULOUSE, ROAD);
    addTransportLink(g, BRUSSELS, COLOGNE, ROAD);
    addTransportLink(g, BRUSSELS, LE_HAVRE, ROAD);
    addTransportLink(g, BRUSSELS, PARIS, ROAD);
    addTransportLink(g, BRUSSELS, STRASBOURG, ROAD);
    addTransportLink(g, BUCHAREST, CONSTANTA, ROAD);
    addTransportLink(g, BUCHAREST, GALATZ, ROAD);
    addTransportLink(g, BUCHAREST, KLAUSENBURG, ROAD);
    addTransportLink(g, BUCHAREST, SOFIA, ROAD);
    addTransportLink(g, BUDAPEST, KLAUSENBURG, ROAD);
    addTransportLink(g, BUDAPEST, SZEGED, ROAD);
    addTransportLink(g, BUDAPEST, VIENNA, ROAD);
    addTransportLink(g, BUDAPEST, ZAGREB, ROAD);
    addTransportLink(g, CADIZ, GRANADA, ROAD);
    addTransportLink(g, CADIZ, LISBON, ROAD);
    addTransportLink(g, CADIZ, MADRID, ROAD);
    addTransportLink(g, CASTLE_DRACULA, GALATZ, ROAD);
    addTransportLink(g, CASTLE_DRACULA, KLAUSENBURG, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, GENEVA, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, MARSEILLES, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, NANTES, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, PARIS, ROAD);
    addTransportLink(g, CLERMONT_FERRAND, TOULOUSE, ROAD);
    addTransportLink(g, COLOGNE, FRANKFURT, ROAD);
    addTransportLink(g, COLOGNE, HAMBURG, ROAD);
    addTransportLink(g, COLOGNE, LEIPZIG, ROAD);
    addTransportLink(g, COLOGNE, STRASBOURG, ROAD);
    addTransportLink(g, CONSTANTA, GALATZ, ROAD);
    addTransportLink(g, CONSTANTA, VARNA, ROAD);
    addTransportLink(g, DUBLIN, GALWAY, ROAD);
    addTransportLink(g, EDINBURGH, MANCHESTER, ROAD);
    addTransportLink(g, FLORENCE, GENOA, ROAD);
    addTransportLink(g, FLORENCE, ROME, ROAD);
    addTransportLink(g, FLORENCE, VENICE, ROAD);
    addTransportLink(g, FRANKFURT, LEIPZIG, ROAD);
    addTransportLink(g, FRANKFURT, NUREMBURG, ROAD);
    addTransportLink(g, FRANKFURT, STRASBOURG, ROAD);
    addTransportLink(g, GALATZ, KLAUSENBURG, ROAD);
    addTransportLink(g, GENEVA, MARSEILLES, ROAD);
    addTransportLink(g, GENEVA, PARIS, ROAD);
    addTransportLink(g, GENEVA, STRASBOURG, ROAD);
    addTransportLink(g, GENEVA, ZURICH, ROAD);
    addTransportLink(g, GENOA, MARSEILLES, ROAD);
    addTransportLink(g, GENOA, MILAN, ROAD);
    addTransportLink(g, GENOA, VENICE, ROAD);
    addTransportLink(g, GRANADA, MADRID, ROAD);
    addTransportLink(g, HAMBURG, LEIPZIG, ROAD);
    addTransportLink(g, KLAUSENBURG, SZEGED, ROAD);
    addTransportLink(g, LEIPZIG, NUREMBURG, ROAD);
    addTransportLink(g, LE_HAVRE, NANTES, ROAD);
    addTransportLink(g, LE_HAVRE, PARIS, ROAD);
    addTransportLink(g, LISBON, MADRID, ROAD);
    addTransportLink(g, LISBON, SANTANDER, ROAD);
    addTransportLink(g, LIVERPOOL, MANCHESTER, ROAD);
    addTransportLink(g, LIVERPOOL, SWANSEA, ROAD);
    addTransportLink(g, LONDON, MANCHESTER, ROAD);
    addTransportLink(g, LONDON, PLYMOUTH, ROAD);
    addTransportLink(g, LONDON, SWANSEA, ROAD);
    addTransportLink(g, MADRID, SANTANDER, ROAD);
    addTransportLink(g, MADRID, SARAGOSSA, ROAD);
    addTransportLink(g, MARSEILLES, MILAN, ROAD);
    addTransportLink(g, MARSEILLES, TOULOUSE, ROAD);
    addTransportLink(g, MARSEILLES, ZURICH, ROAD);
    addTransportLink(g, MILAN, MUNICH, ROAD);
    addTransportLink(g, MILAN, VENICE, ROAD);
    addTransportLink(g, MILAN, ZURICH, ROAD);
    addTransportLink(g, MUNICH, NUREMBURG, ROAD);
    addTransportLink(g, MUNICH, STRASBOURG, ROAD);
    addTransportLink(g, MUNICH, VENICE, ROAD);
    addTransportLink(g, MUNICH, VIENNA, ROAD);
    addTransportLink(g, MUNICH, ZAGREB, ROAD);
    addTransportLink(g, MUNICH, ZURICH, ROAD);
    addTransportLink(g, NANTES, PARIS, ROAD);
    addTransportLink(g, NAPLES, ROME, ROAD);
    addTransportLink(g, NUREMBURG, PRAGUE, ROAD);
    addTransportLink(g, NUREMBURG, STRASBOURG, ROAD);
    addTransportLink(g, PARIS, STRASBOURG, ROAD);
    addTransportLink(g, PRAGUE, VIENNA, ROAD);
    addTransportLink(g, SALONICA, SOFIA, ROAD);
    addTransportLink(g, SALONICA, VALONA, ROAD);
    addTransportLink(g, SANTANDER, SARAGOSSA, ROAD);
    addTransportLink(g, SARAGOSSA, TOULOUSE, ROAD);
    addTransportLink(g, SARAJEVO, SOFIA, ROAD);
    addTransportLink(g, SARAJEVO, ST_JOSEPH_AND_ST_MARYS, ROAD);
    addTransportLink(g, SARAJEVO, VALONA, ROAD);
    addTransportLink(g, SARAJEVO, ZAGREB, ROAD);
    addTransportLink(g, SOFIA, VALONA, ROAD);
    addTransportLink(g, SOFIA, VARNA, ROAD);
    addTransportLink(g, STRASBOURG, ZURICH, ROAD);
    addTransportLink(g, ST_JOSEPH_AND_ST_MARYS, SZEGED, ROAD);
    addTransportLink(g, ST_JOSEPH_AND_ST_MARYS, ZAGREB, ROAD);
    addTransportLink(g, SZEGED, ZAGREB, ROAD);
    addTransportLink(g, VIENNA, ZAGREB, ROAD);

    //### BOAT Connections ###

    addTransportLink(g, ADRIATIC_SEA, BARI, BOAT);
    addTransportLink(g, ADRIATIC_SEA, IONIAN_SEA, BOAT);
    addTransportLink(g, ADRIATIC_SEA, VENICE, BOAT);
    addTransportLink(g, ALICANTE, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, AMSTERDAM, NORTH_SEA, BOAT);
    addTransportLink(g, ATHENS, IONIAN_SEA, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, BAY_OF_BISCAY, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, CADIZ, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, ENGLISH_CHANNEL, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, GALWAY, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, IRISH_SEA, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, LISBON, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, ATLANTIC_OCEAN, NORTH_SEA, BOAT);
    addTransportLink(g, BARCELONA, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, BAY_OF_BISCAY, BORDEAUX, BOAT);
    addTransportLink(g, BAY_OF_BISCAY, NANTES, BOAT);
    addTransportLink(g, BAY_OF_BISCAY, SANTANDER, BOAT);
    addTransportLink(g, BLACK_SEA, CONSTANTA, BOAT);
    addTransportLink(g, BLACK_SEA, IONIAN_SEA, BOAT);
    addTransportLink(g, BLACK_SEA, VARNA, BOAT);
    addTransportLink(g, CAGLIARI, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, CAGLIARI, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, DUBLIN, IRISH_SEA, BOAT);
    addTransportLink(g, EDINBURGH, NORTH_SEA, BOAT);
    addTransportLink(g, ENGLISH_CHANNEL, LE_HAVRE, BOAT);
    addTransportLink(g, ENGLISH_CHANNEL, LONDON, BOAT);
    addTransportLink(g, ENGLISH_CHANNEL, NORTH_SEA, BOAT);
    addTransportLink(g, ENGLISH_CHANNEL, PLYMOUTH, BOAT);
    addTransportLink(g, GENOA, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, HAMBURG, NORTH_SEA, BOAT);
    addTransportLink(g, IONIAN_SEA, SALONICA, BOAT);
    addTransportLink(g, IONIAN_SEA, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, IONIAN_SEA, VALONA, BOAT);
    addTransportLink(g, IRISH_SEA, LIVERPOOL, BOAT);
    addTransportLink(g, IRISH_SEA, SWANSEA, BOAT);
    addTransportLink(g, MARSEILLES, MEDITERRANEAN_SEA, BOAT);
    addTransportLink(g, MEDITERRANEAN_SEA, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, NAPLES, TYRRHENIAN_SEA, BOAT);
    addTransportLink(g, ROME, TYRRHENIAN_SEA, BOAT);
}
