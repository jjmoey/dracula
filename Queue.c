//A linked list implementation of a queue

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#include "Game.h"
#include "GameView.h"
#include "Globals.h"
#include "Map.h"
#include "Queue.h"

typedef struct queueNode * link;

struct queueNode {
    Item item;
    link next;
};

struct queueImp {
    link head;
    link tail;
    int size;
};

static link createNode(Item item){
    link n = malloc (sizeof(*n));
    assert(n != NULL);
    n->item = item;
    n->next = NULL;
    return n;
}

// Creates an empty Queue
Queue createQueue (void){
    Queue q = malloc (sizeof (*q));
    assert(q != NULL);
    q->head = NULL;
    q->tail = NULL;
    q->size = 0;
    return q;
}

Queue copyQueue (Queue queue) {
    Queue q = malloc(sizeof (*q));
    assert(q != NULL);

    q->head = NULL;
    q->tail = NULL;
    q->size = 0;

    // link n = NULL;
    link curr = queue->head;
    while(curr != NULL) {
        enQueue(q, curr->item);
        curr = curr->next;
    }

    return q;
}

void destroyQueue(Queue queue){
    link curr;
    link next;
    assert(queue != NULL);
    curr = queue->head;
    while(curr!=NULL){
        next = curr->next;
        free(curr);
        curr = next;
    }
    free(queue);

}

int queueSize (Queue q){
    assert(q != NULL);
    return q->size;
}


void enQueue (Queue q, Item item){
    assert(q != NULL);
    link n = createNode(item);
    if(q->head != NULL){
       q->tail->next = n;
       q->tail = n;
    } else {
       q->head = n;
       q->tail = n;
    }
    q->size++;

}

Item deQueue (Queue q){
    assert(q != NULL);
    Item item = NOWHERE;
    if (q->head) {
        item = q->head->item;
        link delNode = q->head;
        q->head = q->head->next;
        q->size--;
        free(delNode);
    } else {
        item = NOWHERE;
    }
    return item;

}

Item getHead (Queue q) {
    assert(q != NULL);
    if (q->head) return q->head->item;
    else return NOWHERE;
}

Item getTail (Queue q) {
    assert(q != NULL);
    if (q->tail) return q->tail->item;
    else return UNKNOWN_LOCATION;
}

void printQueueItem(Item item) {
    printf("[%d]->", item);
}

void traverseQueue(Queue queue) {

}

bool queueFind(Queue q, LocationID i) {
    assert (q != NULL); 
    
    link curr = q->head; 
    while (curr != NULL) {
        if (curr->item == i) {
            return true; 
        } 
        curr = curr->next; 
    }
    
    return false; 
}

bool queueHasItems(Queue q) { 
    return (q->head != NULL) ; 
}


