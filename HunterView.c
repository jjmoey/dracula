// HunterView.c ... HunterView ADT implementation
// COMP1927 16s2 ... basic HunterView (supplied code)
// Code by TheGroup from COMP1927 14s2 (modified by gac & jas & angf)

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "Globals.h"
#include "Game.h"
#include "GameView.h"
#include "HunterView.h"
#include "Places.h"
#include "Queue.h"
#include "Map.h"


// Representation of the Hunter's view of the game

struct hunterView {
    GameView game;
    LocationID dracTrail[TRAIL_SIZE]; // real locations
    PlayerMessage * messages;
};

static void setDracTrail(char *pastPlays, LocationID *myTrail);


// Creates a new HunterView to summarise the current state of the game
HunterView newHunterView(char *pastPlays, PlayerMessage messages[])
{
    HunterView hunterView = malloc(sizeof(struct hunterView));
    hunterView->game = newGameView(pastPlays, messages);
    setDracTrail(pastPlays, hunterView->dracTrail);
    return hunterView;
}


// Frees all memory previously allocated for the HunterView toBeDeleted
void disposeHunterView(HunterView toBeDeleted)
{
    free(toBeDeleted->game);
    free(toBeDeleted);
}


//// Functions to return simple information about the current state of the game

// Get the current round
Round giveMeTheRound(HunterView currentView)
{
    return getRound(currentView->game);
}

// Get the id of current player
PlayerID whoAmI(HunterView currentView)
{
    return getCurrentPlayer(currentView->game);
}

// Get the current score
int giveMeTheScore(HunterView currentView)
{
    return getScore(currentView->game);
}

// Get the current health points for a given player
int howHealthyIs(HunterView currentView, PlayerID player)
{
    return getHealth(currentView->game, player);
}



// Get the current location id of a given player
LocationID whereIs(HunterView currentView, PlayerID player)
{
    if(player == PLAYER_DRACULA){
        return currentView->dracTrail[0];
    }
    return getLocation(currentView->game, player);
}

//// Functions that return information about the history of the game

// Fills the trail array with the location ids of the last 6 turns
void giveMeTheTrail(HunterView currentView, PlayerID player,
                    LocationID trail[TRAIL_SIZE])
{
    getHistory(currentView->game, player, trail);
}

//// Functions that query the map to find information about connectivity

// What are my possible next moves (locations)
LocationID *whereCanIgo(HunterView currentView, int *numLocations,
                        bool road, bool rail, bool sea)
{
    return whereCanTheyGo(currentView,
	                      numLocations,
	                      getCurrentPlayer(currentView->game),
	                      road, rail, sea);
}

static int moveInTrail(LocationID trail[TRAIL_SIZE], LocationID move)
{
    for (int i = 0; i < TRAIL_SIZE - 1; i++) {
        if (trail[i] == move) {
            return true;
        }
    }
    return false;
}

static void swap(int index1, int index2, LocationID *conns) {
    LocationID temp = conns[index1];
    conns[index1] = conns[index2];
    conns[index2] = temp;
}

static void removeForbiddenLocations(HunterView dv, LocationID *conns,
                     LocationID trail[TRAIL_SIZE],
                     int *numLocations)
{
    int i = 0;
    while (i < *numLocations) {
        if (whereIs(dv, PLAYER_DRACULA) != conns[i] &&
                moveInTrail(trail, conns[i])) {
            swap(i, --*numLocations, conns);
        } else {
            i++;
        }
    }
}

// What are the specified player's next possible moves
LocationID *whereCanTheyGo(HunterView currentView, int *numLocations,
                           PlayerID player, bool road, bool rail, bool sea)
{
    // printf("calling conn\n");
    LocationID *validLocations = NULL;
    LocationID startLoc = whereIs(currentView,player);
    if(validPlace(startLoc) == false){
        *numLocations = 0;
        return validLocations;
    }

    Round round =getRound(currentView->game);
    // If the given player already had a turn this round, their
    // next move will be during the next round
    if (player < whoAmI(currentView)) round++;

    LocationID *locations =
		connectedLocations(currentView->game,
	                       numLocations,
		                   startLoc,
		                   player,
		                   round,
		                   road, rail, sea);

    if (player == PLAYER_DRACULA) {
        removeForbiddenLocations(currentView, locations, currentView->dracTrail, numLocations);
    }

    return locations;

}

// Sets dracula's trail to real places (resolving TP's, HI's and Dn's)
static void setDracTrail(char *pastPlays, LocationID *myTrail){
	int i;
    for (i = 0; i < TRAIL_SIZE; i++) myTrail[i] = NOWHERE;
	char *end = pastPlays + strlen(pastPlays) - 1;
    // we jump from Drac move to Drac move
    char *p;
    for (p = &pastPlays[32]; p < end; p += 40) {
        // printf("p %s\n",p);
        //if(p[0] != 'D') continue;
        // shift trail to right by one
        LocationID realLoc = NOWHERE; //= myTrail[TRAIL_SIZE];

        if (p[1] == 'T' && p[2] == 'P')      realLoc = CASTLE_DRACULA;
        else if (p[1] == 'H' && p[2] == 'I' && myTrail[0] < NUM_MAP_LOCATIONS)
            realLoc = myTrail[0];
        else if (p[1] == 'D' && p[2] == '1' && myTrail[0] < NUM_MAP_LOCATIONS)
            realLoc = myTrail[0];
        else if (p[1] == 'D' && p[2] == '2' && myTrail[1] < NUM_MAP_LOCATIONS)
            realLoc = myTrail[1];
        else if (p[1] == 'D' && p[2] == '3' && myTrail[2] < NUM_MAP_LOCATIONS)
            realLoc = myTrail[2];
        else if (p[1] == 'D' && p[2] == '4' && myTrail[3] < NUM_MAP_LOCATIONS)
            realLoc = myTrail[3];
        else if (p[1] == 'D' && p[2] == '5' && myTrail[4] < NUM_MAP_LOCATIONS)
            realLoc = myTrail[4];
        else {
            // must be a real location
            char place[3] = { p[1], p[2], '\0' };
            realLoc = abbrevToIDExtended(place);
            if (realLoc == NOWHERE) fprintf(stderr,"Bugger:%s\n",place);
        }

        for (i = TRAIL_SIZE-1; i > 0; i--) {
            myTrail[i] = myTrail[i-1];
        }
        myTrail[0] = realLoc;
    }
    return;
}

void giveMeMessages(HunterView hv, PlayerMessage *messages) {
    int i;
	int turn = NUM_PLAYERS;
	for (i=0;i<turn;i++) {
		//printf("message[%d]: %s\n",i,h->stored_messages[i]);
		strncpy(messages[i], hv->messages[i], MESSAGE_SIZE);
		//printf("message[%d]: %s\n",i,messages[i]);
	}
}

Map getHunterMap (HunterView hv) {
    return getGameMap(hv->game);
}

bool isHunterDead(HunterView h, int player) {
    return getIsDead(h->game, player);
}

int findHunterPath(HunterView h, Vertex src, Vertex dest, int *path, bool road, bool rail, bool sea) {
	printf("finding path from %s to %s\n",idToName(src),idToName(dest));
	if(src==dest) {
		printf("trying to get to where you are\n");
		path[1] = dest;
		return 1;
	}
	int tmp_city = src;
	// Temporary store of path_distance for calculations
	int tmp_distance = 0;
	int path_distance = 0;

	// Array of visited cities, if not visited 0, else 1
	int visited[NUM_MAP_LOCATIONS] = {0};

	// Stores index of the previous city, default value -1
	int prev[NUM_MAP_LOCATIONS+1] = {[0 ... (NUM_MAP_LOCATIONS-1)] = -1};

	Queue cityQ = createQueue();
	enQueue(cityQ, src);

	// While Queue is not empty and the tmp_city is not the destination city (e.g. when path to destination city from src is found)
	while (queueHasItems(cityQ) && tmp_city != dest) {
		tmp_city = deQueue(cityQ);

		int num_locs;
		int *locs = connectedLocations(h->game, &num_locs,tmp_city, whoAmI(h), giveMeTheRound(h),road,rail,sea);

		int i;
		for (i=0;i<num_locs;i++) {


			if (!visited[locs[i]]) {
				enQueue(cityQ, locs[i]);
				prev[locs[i]] = tmp_city;
				visited[locs[i]] = 1;
			}
		}

		if (tmp_city == dest) {
			// setbuf(stdout, NULL);
			// printf("num_locs = %d\n", num_locs);

			/*for(int i =0; i < num_locs; i++) {
				printf("i = %d; ", i);
				printf("locs[i] = %d (%s)\n", locs[i], idToName(locs[i]));
			}

			printf("----\n");
			printf("i = %d; ", i);
			printf("locs[i] = %d (%s)\n", locs[i], idToName(locs[i])); */

			locs[i] = 71;

			prev[locs[i]] = tmp_city;

			// Calculating size of path
			int index = locs[i];
			while (index != src) {
				index = prev[index];
				path_distance++;
			}

			// Building path array, store destination first
			tmp_distance = path_distance-1;
			path[tmp_distance] = dest;
			tmp_distance--;

			// Storing rest of array
			index = prev[dest];
			while (tmp_distance >= 0) {
				path[tmp_distance] = index;
				index = prev[index];
				tmp_distance--;
			}
			break;
		}
	}

    printf("prev: ");
    for (int i = 0; i < NUM_MAP_LOCATIONS; i++) {
        printf("%d - ", prev[i]);
    }


	/*printf("path->");
	int j;
	for(j=0;j<path_distance;j++) {
		printf("[%s (%d)]->",idToName(path[j]), path[j]);
	}
	printf("x\n"); */

	return path_distance;
}
