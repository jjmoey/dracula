////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// dracula.c: your "Fury of Dracula" Dracula AI
//
// 2014-07-01   v1.0    Team Dracula <cs2521@cse.unsw.edu.au>
// 2017-12-01   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>

#include "DracView.h"
#include "Game.h"
#include "dracula.h"

#define DRACULA_START_POS MUNICH

/*
    General strategy:
        Random location function built in
        DEFAULT: pick start position and randomly move

*/

LocationID makeRandomMove(DracView dv);
static bool inTrail(int trail[TRAIL_SIZE], int location);
static bool teleportInTrail(int trail[TRAIL_SIZE]);
static LocationID convertTrail(int trail[TRAIL_SIZE], int location);

Graph gameMap = NULL;

/*
Basic strategy: avoid other players by finding the closest distance to a plyaer
Find a location furthest away from the closest player and move there
*/

void
decideDraculaMove (DracView dv)
{
    gameMap = getDracMap(dv);
    LocationID move;
    int currRound = giveMeTheRound(dv);
    LocationID dracloc = whereIs(dv, PLAYER_DRACULA);

    int dracTrail[TRAIL_SIZE];
	giveMeTheTrail(dv, PLAYER_DRACULA, dracTrail);

    if (currRound == 0) {
        move = DRACULA_START_POS;
    } else {
        int numLocations = 0;
        int distances[NUM_PLAYERS-1] = {0};
        int health[NUM_PLAYERS-1] = {0};
        LocationID *goDrac = whereCanIgo(dv, &numLocations, true, true);
        printf("dracpath: ");
        for (int i = 0; i < numLocations; i++) {
            printf("[%s]->", idToName(goDrac[i]));
        }
        printf("[x]\n");

        for (int i = 0; i < NUM_PLAYERS-1; i++) {
            distances[i] = findDist(gameMap, dracloc, whereIs(dv, i));
            health[i] = howHealthyIs(dv, i);
        }

        if (numLocations > 0) {
            int min, temp = 0;
            PlayerID closest = PLAYER_LORD_GODALMING;

            for (int i = 0; i < NUM_PLAYERS-1; i++) {
                min = distances[i] < min ? distances[i] : min;
                closest = distances[i] < min ? i : closest;
            }

            LocationID furthestFromHunter = goDrac[0];
            min = findDist(gameMap, dracloc, whereIs(dv, closest));
            printf("# possible locations: %d\n", numLocations);
            printf("initial location: %d\n", furthestFromHunter);
            for (int i = 1; i < numLocations; i++) {
                temp = findDist(gameMap, goDrac[i], whereIs(dv, closest));
                min = temp < min ? temp : min;
                furthestFromHunter = temp < min ? i : goDrac[i];
            }

            move = furthestFromHunter;

        } else if (numLocations == 0 && teleportInTrail(dracTrail)) {
            move = TELEPORT;
        } else {
            move = HIDE;
        }

    }

    if (inTrail(dracTrail, move) && !teleportInTrail(dracTrail)) {
        printf("previous location exists, converting to double back");
        move = convertTrail(dracTrail, move);
    } else if (!teleportInTrail(dracTrail)) {

    }

    char * moveTo = idToAbbrev(move);
    registerBestPlay (moveTo, "");
}

static bool teleportInTrail(int trail[TRAIL_SIZE]) {
	int i, in = true;

	for (i = 0; i < TRAIL_SIZE-1; i++) {
		if (trail[i] == 108) {
			in = true;
		}
	}

	return in;
}


static bool inTrail(int trail[TRAIL_SIZE], int location) {
	int i;
	int in = false;
	// Doesn't include last move on the trail, can go there because it will drop off
	for (i = 0; i < TRAIL_SIZE-1; i++) {
		if (location == trail[i]) {
			in = true;
		}
	}
	return in;
}

LocationID makeRandomMove(DracView dv) {
    int numLocations;
	int *locs = whereCanIgo(dv,&numLocations,true,true);

    printf("locs (#locs = %d) ", numLocations);
    for (int i = 0; i < numLocations; i++) {
        printf("[%s] - ", idToName(locs[i]));
    }
    printf("\n");
	int r = (rand()%(numLocations-1))+1; //so we don't select current loc (element 0)
    if (locs != NULL && locs[r] != UNKNOWN_LOCATION) {
        // printf("eer %s\n", idToName(locs[r]));
	    return locs[r];
    } else {
        return whereIs(dv, PLAYER_DRACULA);
    }
}

static LocationID convertTrail(int trail[TRAIL_SIZE], int location) {

	int i;

	bool hasHide = false;
	bool hasDouble = false;
	int howFarBack = 0;
	LocationID converted = UNKNOWN_LOCATION;

	// Check for hide or double back in the trail, ignore last element as can go there without double back and hide
	for (i = 0; i < TRAIL_SIZE-1; i++) {
		if (trail[i] == HIDE) {
			hasHide = true;
		} else if (trail[i] > HIDE && trail[i] < TELEPORT) {
			hasDouble = true;
		}
	}

	for (i = 0; i < TRAIL_SIZE-1; i++) {
		if (location == trail[i]) {
			howFarBack = i;
			break;
		}
	}

	// if there is a double back in the first place of the trail, hide is not possible

	// If there is a double back in trail and the location is also in the trail, has to mean hide
	if (hasDouble) {
		converted = HIDE;
	} else if (hasHide) {
		switch (howFarBack) {
			case 0:
				converted = DOUBLE_BACK_1;
				break;
			case 1:
				converted = DOUBLE_BACK_2;
				break;
			case 2:
				converted = DOUBLE_BACK_3;
				break;
			case 3:
				converted = DOUBLE_BACK_4;
				break;
			case 4:
				converted = DOUBLE_BACK_5;
				break;
		}
	} else {
		// If there is neither hide or double back in trail
		if (trail[0] == location) {
			converted = HIDE;
		} else {
			switch (howFarBack) {
				case 0:
					converted = DOUBLE_BACK_1;
					break;
				case 1:
					converted = DOUBLE_BACK_2;
					break;
				case 2:
					converted = DOUBLE_BACK_3;
					break;
				case 3:
					converted = DOUBLE_BACK_4;
					break;
				case 4:
					converted = DOUBLE_BACK_5;
					break;
			}
		}
	}

	return converted;
}
